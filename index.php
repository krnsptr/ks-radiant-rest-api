<?php

require './App/Controller/NodeController.php';

use App\Controller\NodeController;

$uri = strtok($_SERVER['REQUEST_URI'], '?');
$uri = trim($uri, '/');

$routes = [
    'api/ping' => [NodeController::class, 'ping'],
    'api/node' => [NodeController::class, 'api'],
];

foreach ($routes as $pattern => $route) {
    if (stripos($uri, $pattern) === 0) {
        $controller = $route[0] ?? null;
        $method = $route[1] ?? null;
        $uri = trim(substr($uri, strlen($pattern)), '/');
        break;
    }
}

if (!isset($controller, $method)
    || !class_exists($controller)
    || !method_exists($controller, $method)
) {
    http_response_code(404);
    header('Content-type: application/json');
    echo json_encode(['message' => 'Not found']);
    exit;
}

call_user_func_array([new $controller(), $method], [$uri]);
