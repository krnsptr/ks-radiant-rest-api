<?php

namespace App\Controller;

use PDO;

class NodeController
{
    private PDO $pdo;

    public function __construct()
    {
        $this->connect();
        $this->setup();
    }

    private function connect()
    {
        $this->pdo = new PDO('sqlite:database.sqlite', null, null, [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ]);
    }

    private function setup()
    {
        $this->pdo->exec(
            'CREATE TABLE IF NOT EXISTS nodes
            (id INTEGER PRIMARY KEY AUTOINCREMENT, parent_id INTEGER NULL);'
        );

        $rows = $this->pdo->query('SELECT COUNT(*) AS count FROM nodes')->fetchColumn();

        if ($rows < 1) {
            $this->pdo->exec(
                'INSERT INTO nodes (id, parent_id) VALUES (1, NULL);
                INSERT INTO nodes (id, parent_id) VALUES (2, 1);
                INSERT INTO nodes (id, parent_id) VALUES (3, 1);
                INSERT INTO nodes (id, parent_id) VALUES (4, 1);
                INSERT INTO nodes (id, parent_id) VALUES (5, 2);
                INSERT INTO nodes (id, parent_id) VALUES (6, 2);
                INSERT INTO nodes (id, parent_id) VALUES (7, 3);
                INSERT INTO nodes (id, parent_id) VALUES (8, 4);
                INSERT INTO nodes (id, parent_id) VALUES (9, 4);
                INSERT INTO nodes (id, parent_id) VALUES (10, 4);
                INSERT INTO nodes (id, parent_id) VALUES (11, 5);
                INSERT INTO nodes (id, parent_id) VALUES (12, 5);
                INSERT INTO nodes (id, parent_id) VALUES (13, 7);
                INSERT INTO nodes (id, parent_id) VALUES (14, 7);
                INSERT INTO nodes (id, parent_id) VALUES (15, 13);'
            );
        }
    }

    private function responseJson(int $status, array $data)
    {
        http_response_code($status);
        header('Content-type: application/json');
        echo json_encode($data);
    }

    private function responseMessage(int $status, string $message)
    {
        $this->responseJson($status, compact('message'));
    }

    public function ping($uri)
    {
        $this->responseJson(200, ['message' => 'OK', 'uri' => $uri]);
    }

    public function api($uri)
    {
        $verb = strtoupper($_SERVER['REQUEST_METHOD']);

        if ($verb === 'GET' && $uri === '') {
            return call_user_func_array([$this, 'index'], []);
        }

        if ($verb === 'GET' && $uri !== '') {
            return call_user_func_array([$this, 'get'], [$uri]);
        }

        if ($verb === 'POST' && $uri === '') {
            return call_user_func_array([$this, 'create'], []);
        }

        if ($verb === 'PATCH' && $uri !== '') {
            return call_user_func_array([$this, 'update'], [$uri]);
        }

        if ($verb === 'DELETE' && $uri !== '') {
            return call_user_func_array([$this, 'delete'], [$uri]);
        }

        return $this->responseMessage(404, 'Invalid action');
    }

    private function index()
    {
        $stmt = $this->pdo->prepare('SELECT * FROM nodes');
        $stmt->execute();
        $rows = (array) $stmt->fetchAll();

        $this->responseJson(200, [
            'message' => 'OK',
            'result' => $rows,
        ]);
    }

    private function get($id, $message = null)
    {
        $id = intval($id);
        $stmt = $this->pdo->prepare('SELECT * FROM nodes WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch();

        if (empty($row)) {
            return $this->responseMessage(404, 'Not found');
        }

        $this->responseJson(200, [
            'message' => $message ?? 'OK',
            'result' => $row,
        ]);
    }

    private function create()
    {
        $body = json_decode(file_get_contents('php://input'));
        $parent_id = $body->parent_id ?? null;

        $stmt = $this->pdo->prepare('INSERT INTO nodes (parent_id) VALUES (:parent_id)');
        $stmt->bindParam(':parent_id', $parent_id);
        $success = $stmt->execute();

        if (!$success) {
            return $this->responseMessage('400', 'Error');
        }

        $id = $this->pdo->lastInsertId();
        return $this->get($id, "Node $id created");
    }

    private function update($id)
    {
        $id = intval($id);
        $body = json_decode(file_get_contents('php://input'));
        $parent_id = $body->parent_id ?? null;

        $stmt = $this->pdo->prepare('UPDATE nodes SET parent_id = :parent_id WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':parent_id', $parent_id);
        $success = $stmt->execute();

        if (!$success) {
            return $this->responseMessage('400', 'Error');
        }

        return $this->get($id, "Node $id updated");
    }

    private function delete($id)
    {
        $id = intval($id);
        $stmt = $this->pdo->prepare('DELETE FROM nodes WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $success = $stmt->execute();

        if (!$success) {
            return $this->responseMessage('400', 'Error');
        }

        return $this->responseMessage(200, "Node $id deleted");
    }
}
